# Addon Info
bl_info = {
	"name": "Real Sky",
	"description": "Physical sky lighting and 3D procedural clouds",
	"author": "Wolf",
	"version": (1, 9),
	"blender": (2, 83, 0),
	"location": "View 3D > Properties Panel",
	"doc_url": "https://3d-wolf.com/products/sky",
	"tracker_url": "https://3d-wolf.com/products/sky",
	"support": "COMMUNITY",
	"category": "Lighting"
	}


# Libraries
from mathutils import Vector
import numpy as np
import os
from bpy.app.handlers import persistent
from bpy.props import BoolProperty, EnumProperty, FloatProperty, IntProperty, PointerProperty
from bpy.types import Panel, PropertyGroup
from bpy.utils import previews
from . import spectrum
from . import billboard

import bpy
from math import acos, asin, atan, cos, degrees, exp, pi, pow, radians, sin
import bmesh


# Globals
AU = 149597870700 # Astronomical Unit
year = 365.256 # Days on a Year
e = 0.0167 # Eccentricity
d = 1391400000 # Sun's Diameter
R = 695700000 # Sun's Radius
L = 3.828*pow(10, 26)# Sun's Luminosity
o = 5.670373*pow(10, -8)# Stefan Boltzmann constant
T = 5778 # Sun's Temperature
a = 0.14
clouds_icons = None


# Panel Sky
class REALSKY_PT_Sky(Panel):
	bl_space_type = "VIEW_3D"
	bl_context = "objectmode"
	bl_region_type = "UI"
	bl_label = "Sky"
	bl_category = "Real Sky"

	def draw_header(self, context):
		settings = context.scene.sky_settings
		layout = self.layout
		layout.prop(settings, 'enabled', text='')

	def draw(self, context):
		settings = context.scene.sky_settings
		layout = self.layout
		layout.enabled = settings.enabled

		# Settings
		col = layout.column(align=True)
		col.label(text="Sun", icon='LIGHT_SUN')
		col.prop(settings, 'direction')
		col = layout.column()
		sub = col.column(align=True)
		row = layout.row()
		row = sub.row(align=True)
		row.prop(settings, 'month')
		if settings.month==2:
			row.prop(settings, 'day28')
		if settings.month in (4, 6, 9, 11):
			row.prop(settings, 'day30')
		if settings.month in (1, 3, 5, 7, 8, 10, 12):
			row.prop(settings, 'day31')
		sub.prop(settings, 'time')
		sub.prop(settings, 'latitude')
		col.label(text="")

		col = layout.column()
		row = layout.row()
		row = col.row(align=True)
		row.label(text="Sky", icon='WORLD_DATA')
		col = layout.column()
		col.prop(settings, 'sky_method', text='')
		sub = col.column(align=True)
		sub.prop(settings, 'altitude')
		sub.prop(settings, 'turbidity', slider=True)
		sub.prop(settings, 'albedo', slider=True)


# Panel Clouds
class REALSKY_PT_Clouds(Panel):
	bl_space_type = "VIEW_3D"
	bl_context = "objectmode"
	bl_region_type = "UI"
	bl_label = "Clouds"
	bl_category = "Real Sky"

	def draw(self, context):
		settings = bpy.context.scene.clouds_settings
		layout = self.layout
		sky_en = context.scene.sky_settings.enabled
		global clouds_icons

		# Cirrus
		split = layout.split()
		col = split.column(align=True)
		col.scale_y = 1.2
		col.prop(settings, 'cirrus', icon_value=clouds_icons["cirrus"].icon_id)
		if settings.cirrus:
			col = layout.column()
			col.prop(settings, 'cirrus_direction')
			sub = col.column(align=True)
			sub.prop(settings, 'cirrus_location')
			sub.prop(settings, 'cirrus_coverage', slider=True)
			sub.prop(settings, 'cirrus_density', slider=True)
			col.label(text="")

		# Cirrocumulus
		split = layout.split()
		col = split.column(align=True)
		col.scale_y = 1.2
		col.prop(settings, 'cirrocumulus', icon_value=clouds_icons["cirrocumulus"].icon_id)
		if settings.cirrocumulus:
			col = layout.column()
			col.prop(settings, 'cirrocumulus_direction')
			sub = col.column(align=True)
			sub.prop(settings, 'cirrocumulus_location')
			sub.prop(settings, 'cirrocumulus_coverage', slider=True)
			sub.prop(settings, 'cirrocumulus_density', slider=True)
			col.label(text="")

		# Altostratus
		split = layout.split()
		col = split.column(align=True)
		col.scale_y = 1.2
		col.prop(settings, 'altostratus', icon_value=clouds_icons["altostratus"].icon_id)
		if settings.altostratus:
			col = layout.column()
			row = layout.row()
			row = col.row(align=True)
			row.prop(settings, 'altostratus_method', expand=True)
			if settings.altostratus_method=="Volume":
				row = col.row(align=True)
				if not sky_en:
					row.alignment = "CENTER"
					row.label(text="Enable Sky for mist", icon='ERROR')
				else:
					row.prop(settings, 'altostratus_mist', icon='FREEZE')
			else:
				row = col.row(align=True)
				row.alignment = "CENTER"
				row.label(text="Resolution", icon='MESH_GRID')
				row = col.row(align=True)
				row.prop(settings, 'altostratus_billboard_res', expand=True)
				if not sky_en:
					row = col.row(align=True)
					row.alignment = "CENTER"
					row.label(text="Enable Sky for mist", icon='ERROR')
			row = col.row(align=True)
			row.prop(settings, 'altostratus_direction')
			sub = col.column(align=True)
			sub.prop(settings, 'altostratus_location')
			sub.prop(settings, 'altostratus_coverage', slider=True)
			sub.prop(settings, 'altostratus_density', slider=True)
			col.label(text="")

		# Cumulus
		split = layout.split()
		col = split.column(align=True)
		col.scale_y = 1.2
		col.prop(settings, 'cumulus', icon_value=clouds_icons["cumulus"].icon_id)
		if settings.cumulus:
			col = layout.column()
			row = layout.row()
			row = col.row(align=True)
			row.prop(settings, 'cumulus_method', expand=True)
			if settings.cumulus_method=="Volume":
				row = col.row(align=True)
				if not sky_en:
					row.alignment = "CENTER"
					row.label(text="Enable Sky for mist", icon='ERROR')
				else:
					row.prop(settings, 'cumulus_mist', icon='FREEZE')
			else:
				row = col.row(align=True)
				row.alignment = "CENTER"
				row.label(text="Resolution", icon='MESH_GRID')
				row = col.row(align=True)
				row.prop(settings, 'cumulus_billboard_res', expand=True)
				if not sky_en:
					row = col.row(align=True)
					row.alignment = "CENTER"
					row.label(text="Enable Sky for mist", icon='ERROR')
			row = col.row(align=True)
			row.prop(settings, 'cumulus_direction')
			sub = col.column(align=True)
			sub.prop(settings, 'cumulus_location')
			sub.prop(settings, 'cumulus_coverage', slider=True)
			sub.prop(settings, 'cumulus_density', slider=True)
			col.label(text="")


# Enable Real Sky
def enable_sky(self, context):
	settings = context.scene.sky_settings
	if settings.enabled:
		context.scene.view_settings.exposure = -6
		if "Sun" in bpy.data.objects:
			remove_sun(self, context)
		create_sun(self, context)
		create_sky(self, context)
		sun_calculation(context)
	else:
		remove_sun(self, context)


# Create sun
def create_sun(self, context):
	# Set master collection to active
	context.view_layer.active_layer_collection = context.view_layer.layer_collection
	# Add sun
	bpy.ops.object.light_add(type='SUN')
	context.object.data.name = "Sun"
	context.object.name = "Sun"


def remove_sun(self, context):
	if "Sun" in bpy.data.objects:
		bpy.data.objects.remove(bpy.data.objects["Sun"])


def create_sky(self, context):
	settings = context.scene.sky_settings
	method = settings.sky_method
	# If there is a world, change name
	if method in bpy.data.worlds:
		bpy.data.worlds[method].name = method+".000"
	path = os.path.join(os.path.dirname(__file__), "blends/sky.blend/World/")
	wm = context.window_manager
	bpy.ops.wm.append(directory=path, filepath=method+".blend", filename=method)
	# Assign it to world
	w = bpy.data.worlds[method]
	context.scene.world = w


# Sun
def sun_calculation(context):
	settings = context.scene.sky_settings

	direction = settings.direction
	time = settings.time
	la = settings.latitude
	A = settings.altitude
	Turbidity = settings.turbidity

	# Day
	day = calculate_day(context)
	# Distance Earth-Sun
	distance = (1-e*cos((360/year)*(day-4)*(pi/180)))*AU
	# Angular Diameter (Blender Sun Size)
	angular = 2*atan(d/(2*distance))
	# Declination
	de = asin(sin(radians(-23.44))*cos(radians((360/365.24)*(day+10)+(360/pi)*0.0167*sin(radians((360/365.24)*(day-2))))))
	# Hour Angle
	w = radians(15*(time-12))
	# Sun Altitude
	hs = asin(cos(la)*cos(de)*cos(w)+sin(la)*sin(de))
	# Sun Zenith
	z = pi/2-hs
	sun_calculation.z = z
	# Sun Azimuth
	if w==0:
		try:
			y = acos((sin(de)-cos(z)*sin(la))/(sin(z)*cos(la))+0.0000001)
		except ValueError:
			y = 0
	else:
		y = acos((sin(de)-cos(z)*sin(la))/(sin(z)*cos(la)))
	if w>0:
		y = 2*pi-y
	# Solar Constant
	Gsc = o*T**4*(R/distance)**2
	# Solar irradiation above atmosphere
	Gon = Gsc*(1+0.033*cos(360*day/365))
	if Gon<0:
		Gon=0
	# Solar irradiance tangent atmosphere
	Go = Gon*cos(z)
	# Air Mass
	AM = (exp(-0.0001184*A))/(cos(z)+0.50572*(96.07995-(90-degrees(hs)))**-1.6364)
	if degrees(hs)<-1.7:
		AM = (exp(-0.0001184*A))/(cos(radians(90+1.7))+0.50572*(96.07995-(90+1.7))**-1.6364)
	# Redness factor
	red = (4/3)*Turbidity
	AM = AM*(2*(red/100)+1)
	#Irradiance at ground
	h = A/1000
	I = Gon*((1-a*h)*0.7**AM**0.678+a*h)

	# Data for Color
	illuminant_D65 = spectrum.xyz_from_xy(0.3127, 0.3291)
	cs_hdtv = spectrum.ColourSystem(red=spectrum.xyz_from_xy(0.67, 0.33),
							green=spectrum.xyz_from_xy(0.21, 0.71),
							blue=spectrum.xyz_from_xy(0.15, 0.06),
							white=illuminant_D65)
	# Wavelengths from 380 to 780 in steps of 5nm
	lam = np.arange(380., 781., 5)
	# Calculate spectrum at ground level
	spec = spectrum.spec_col(lam, AM, distance)
	# Convert Spectrum to RGB
	rgb = spectrum.cs_srgb.spec_to_rgb(spec)
	# Add alpha channel
	rgba = np.append(rgb, 1)
	sun_calculation.rgb = rgba

	# Sun
	sun = bpy.data.objects["Sun"]
	# Sun X Rotation
	sun.rotation_euler[0] = z
	# Sun Z Rotation
	sun.rotation_euler[2] = pi-y-direction
	# Sun Strength
	bpy.data.lights["Sun"].energy = I
	# Sun Color
	bpy.data.lights["Sun"].color = rgb
	sun_calculation.strength = I
	# Sun Size
	bpy.data.lights["Sun"].angle = angular
	# Sun disk strength
	Idisk = 16.5441176*I+900.7352944
	# Sun disk size
	disk = 0.0000561798*(angular*180/pi)+0.999959553371

	# Sky
	sky_calculation(context, settings, sun, Go, hs, rgba, Idisk, disk, I)


# Sky
def sky_calculation(context, settings, sun, Go, hs, rgba, Idisk, disk, I):
	method = settings.sky_method
	A = settings.altitude
	Turbidity = settings.turbidity
	Albedo = settings.albedo
	clouds = context.scene.clouds_settings
	sky = bpy.data.worlds[method].node_tree

	# Sky rotation
	vec = Vector((0, 0, 1))
	vec.rotate(sun.rotation_euler)
	sky_calculation.vec = vec
	# Sun disk rotation
	vec1 = Vector((0, 0, -1))
	vec1.rotate(sun.rotation_euler)
	# Diffuse Irradiance
	A = A/1000 # Altitude in Km
	ao = 0.4237-0.00821*(6-A)**2
	a1 = 0.5055+0.00595*(6.5-A)**2
	k = 0.2711+0.01858*(2.5-A)**2
	if hs<0:
		hs=0
	Tb = ao+a1*exp(-k/cos((pi/2)-hs))
	Td = 0.271-0.294*Tb
	Gd = Go*Td
	if Gd<0.1:
		Gd=0.1
	sky_calculation.gd = Gd
	# Sky ground
	if method=="Real Sky":
		gnd = 94*Gd/103
	else:
		gnd = 1.87*10**-6*Gd**4-4.74*10**-4*Gd**3+0.0364*Gd**2-0.0933*Gd+0.281
	sky_calculation.gnd = gnd

	if method=="Hosek-Wilkie":
		# Sky Texture
		sky.nodes['tex'].sun_direction = vec
		sky.nodes['tex'].turbidity = Turbidity/10
		sky.nodes['tex'].ground_albedo = Albedo/100
		# Sky strength (*5 because of Sky Texture value=0.2)
		sky.nodes['back_sky'].inputs[1].default_value = Gd*5
	else:
		# Albedo
		alb = 0.001*Albedo-0.2
		sky.nodes['albedo'].inputs[1].default_value[2] = alb
		# Sky color
		rgb_sun = [i*2 for i in rgba]
		sky.nodes['sky'].color_ramp.elements[0].color = rgb_sun
		# Mist
		mist = (Turbidity/100)*0.15
		sky.nodes['mist'].inputs[0].default_value = mist
		mist_mul = (Turbidity/100)*-0.5
		sky.nodes['mist_mul'].inputs[1].default_value = mist_mul
		# Glow
		glo_pow = -9.983*Turbidity+1000.1
		glo_mul = 0.0045*Turbidity+0.05
		sky.nodes['glow_pow'].inputs[1].default_value = glo_pow
		sky.nodes['glow_mul'].inputs[1].default_value = glo_mul
		# Sky strength
		sky.nodes['back_sky'].inputs[1].default_value = Gd

	# Sun disk
	sky.nodes['nor'].outputs[0].default_value = vec1
	sky.nodes['great'].inputs[1].default_value = disk
	# Sun glow
	sky.nodes['back_glow'].inputs[0].default_value = rgba
	sky.nodes['back_glow'].inputs[1].default_value = 10.4*pow(I, 0.597)
	sky.nodes['nor_glow'].outputs[0].default_value = vec1
	# Sky ground
	sky.nodes['back_ground'].inputs[1].default_value = gnd
	# Sun disk
	sky.nodes['back_sun'].inputs[0].default_value = rgba
	sky.nodes['back_sun'].inputs[1].default_value = Idisk

	# Update clouds
	if clouds.altostratus:
		if clouds.altostratus_method=="Billboard":
			altostratus_billboard_update(context)
		else:
			if clouds.altostratus_mist:
				altostratus_update(context)
	if clouds.cumulus:
		if clouds.cumulus_method=="Billboard":
			cumulus_billboard_update(context)
		else:
			if clouds.cumulus_mist:
				cumulus_update(context)


def enable_cirrus(self, context):
	bpy.context.area.spaces.active.clip_end = 800000
	settings = context.scene.clouds_settings
	cloud = "cirrus"
	if settings.cirrus:
		create_grid(self, context, cloud)
	else:
		delete_grid(self, context, cloud)


def enable_cirrocumulus(self, context):
	bpy.context.area.spaces.active.clip_end = 800000
	settings = context.scene.clouds_settings
	cloud = "cirrocumulus"
	if settings.cirrocumulus:
		create_grid(self, context, cloud)
	else:
		delete_grid(self, context, cloud)


def enable_altostratus(self, context):
	bpy.context.area.spaces.active.clip_end = 800000
	settings = context.scene.clouds_settings
	method = settings.altostratus_method
	mist = settings.altostratus_mist
	if method=="Volume":
		cloud = "altostratus"
		bpy.context.scene.cycles.volume_bounces = 4
		if mist:
			cloud="altostratus_mist"
	else:
		cloud = "altostratus_billboard"
	if settings.altostratus:
		if bpy.data.objects.get("altostratus") is not None:
			delete_grid(self, context, "altostratus")
		if bpy.data.objects.get("altostratus_mist") is not None:
			delete_grid(self, context, "altostratus_mist")
		if bpy.data.objects.get("altostratus_billboard") is not None:
			delete_grid(self, context, "altostratus_billboard")
		create_grid(self, context, cloud)
	else:
		delete_grid(self, context, cloud)


def enable_cumulus(self, context):
	bpy.context.area.spaces.active.clip_end = 800000
	settings = context.scene.clouds_settings
	method = settings.cumulus_method
	mist = settings.cumulus_mist
	if method=="Volume":
		cloud = "cumulus"
		bpy.context.scene.cycles.volume_bounces = 4
		if mist:
			cloud="cumulus_mist"
	else:
		cloud = "cumulus_billboard"
	if settings.cumulus:
		if bpy.data.objects.get("cumulus") is not None:
			delete_grid(self, context, "cumulus")
		if bpy.data.objects.get("cumulus_mist") is not None:
			delete_grid(self, context, "cumulus_mist")
		if bpy.data.objects.get("cumulus_billboard") is not None:
			delete_grid(self, context, "cumulus_billboard")
		create_grid(self, context, cloud)
	else:
		delete_grid(self, context, cloud)


# Grid
def create_grid(self, context, cloud):
	# Generate Grid
	grid = billboard.generate_grid(self, context, cloud)
	create_grid.obj = grid
	# Assign material
	cloud_material(self, context, cloud)


def delete_grid(self, context, cloud):
	if cloud in bpy.data.objects:
		bpy.data.objects.remove(bpy.data.objects[cloud])


# Apply material to cloud
def cloud_material(self, context, cloud):
	# Select clouds collection
	context.view_layer.active_layer_collection = bpy.context.view_layer.layer_collection.children["Clouds"]
	# If there is a material, change name
	if cloud in bpy.data.materials:
		bpy.data.materials[cloud].name = cloud+".000"
	path = os.path.join(os.path.dirname(__file__), "blends/clouds.blend/Material")
	bpy.ops.wm.append(directory=path, filepath="clouds.blend", filename=cloud)
	# Get material
	mat = bpy.data.materials.get(cloud)
	# Assign it to object
	ob = create_grid.obj
	ob.data.materials.append(mat)
	# Update material
	if cloud=="cirrus":
		cirrus_update(context)
		bpy.context.object.active_material.blend_method = 'BLEND'
	elif cloud=="cirrocumulus":
		cirrocumulus_update(context)
		bpy.context.object.active_material.blend_method = 'BLEND'
	elif cloud=="altostratus" or cloud=="altostratus_mist":
		altostratus_update(context)
	elif cloud=="altostratus_billboard":
		altostratus_billboard_update(context)
		bpy.context.object.active_material.blend_method = 'HASHED'
	elif cloud=="cumulus" or cloud=="cumulus_mist":
		cumulus_update(context)
	elif cloud=="cumulus_billboard":
		cumulus_billboard_update(context)
		bpy.context.object.active_material.blend_method = 'HASHED'


# Clouds update
def altostratus_selector(context):
	method = context.scene.clouds_settings.altostratus_method
	if method=="Volume":
		altostratus_update(context)
	else:
		altostratus_billboard_update(context)


def cumulus_selector(context):
	method = context.scene.clouds_settings.cumulus_method
	if method=="Volume":
		cumulus_update(context)
	else:
		cumulus_billboard_update(context)


def cirrus_update(context):
	settings = context.scene.clouds_settings
	direction = settings.cirrus_direction
	location = settings.cirrus_location/10
	coverage = settings.cirrus_coverage
	density = settings.cirrus_density
	cloud = "cirrus"

	# Direction
	x = cos(direction)*location
	y = sin(direction)*location
	# Location
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[0] = x/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[1] = y/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[0] = -x/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[1] = -y/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[2] = -location/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover 2'].inputs[1].default_value[0] = x/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover 2'].inputs[1].default_value[1] = y/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover 2'].inputs[1].default_value[2] = location/4
	# Coverage
	cov = (0.6*coverage+20)/100
	black = 0.8*(1-cov)
	white = black+0.2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[1].position = white
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage 2'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage 2'].color_ramp.elements[1].position = white
	# Density
	den = density/100
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Density'].inputs[1].default_value = den


def cirrocumulus_update(context):
	settings = context.scene.clouds_settings
	direction = settings.cirrocumulus_direction
	location = settings.cirrocumulus_location/10
	coverage = settings.cirrocumulus_coverage
	density = settings.cirrocumulus_density
	cloud = "cirrocumulus"

	# Direction
	x = cos(direction)*location
	y = sin(direction)*location
	# Location
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[0] = x/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[1] = y/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location 2'].inputs[1].default_value[0] = x/1.5
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location 2'].inputs[1].default_value[1] = y/1.5
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[0] = -x/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[1] = -y/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[2] = -location/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover 2'].inputs[1].default_value[0] = -x/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover 2'].inputs[1].default_value[1] = -y/4
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover 2'].inputs[1].default_value[2] = -location/4
	# Coverage
	cov = (0.6*coverage+20)/100
	black = 0.85*(1-cov)
	white = black+0.15
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[1].position = white
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage 2'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage 2'].color_ramp.elements[1].position = white
	# Density
	den = density/100
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Density'].inputs[1].default_value = den


def altostratus_update(context):
	settings = context.scene.clouds_settings
	direction = settings.altostratus_direction
	location = settings.altostratus_location/10
	coverage = settings.altostratus_coverage
	density = settings.altostratus_density
	mist = settings.altostratus_mist
	if not mist:
		cloud = "altostratus"
	else:
		cloud = "altostratus_mist"

	# Direction
	x = cos(direction)*location
	y = sin(direction)*location
	# Location
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[0] = x
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[1] = y
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[0] = -x/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[1] = -y/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[2] = -location/2
	# Coverage
	cov = (0.8*coverage+10)/100
	black = 0.85*(1-cov)
	white = black+0.15
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[1].position = white
	# Density
	den = density/10000
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Density'].inputs[1].default_value = den
	# Mist
	gd = sky_calculation.gd
	m = (0.1*gd/103)/2
	if mist:
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Mist'].inputs[1].default_value = m


def altostratus_billboard_update(context):
	settings = context.scene.clouds_settings
	sky_en = context.scene.sky_settings.enabled
	direction = settings.altostratus_direction
	location = settings.altostratus_location/10
	coverage = settings.altostratus_coverage
	density = settings.altostratus_density
	cloud="altostratus_billboard"

	# Direction
	x = cos(direction)*location
	y = sin(direction)*location
	# Location
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[0] = x
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[1] = y
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[0] = -x/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[1] = -y/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[2] = -location/2
	# Location Shadow
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Shadow'].inputs[1].default_value[0] = x
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Shadow'].inputs[1].default_value[1] = y
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover Shadow'].inputs[1].default_value[0] = -x/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover Shadow'].inputs[1].default_value[1] = -y/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover Shadow'].inputs[1].default_value[2] = -location/2
	# Coverage
	cov = coverage/100
	black = 0.9*(1-cov)
	white = black+0.1
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[1].position = white
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Remover Shadow'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Remover Shadow'].color_ramp.elements[1].position = white
	# Density
	den = 0.95*(1-density/100)+0.05
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Clouds Color'].color_ramp.elements[1].position = den
	if sky_en:
		# Sun Position
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Sun Position'].outputs[0].default_value = sky_calculation.vec
		# Clouds Color
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Clouds Color'].color_ramp.elements[0].color = sun_calculation.rgb
		# Clouds Strength
		gnd = sky_calculation.gnd
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Clouds Emission'].inputs[1].default_value = gnd*8
		# Mist
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Mist'].inputs[1].default_value = gnd*2


def cumulus_update(context):
	settings = context.scene.clouds_settings
	direction = settings.cumulus_direction
	location = settings.cumulus_location*100
	coverage = settings.cumulus_coverage
	density = settings.cumulus_density
	mist = settings.cumulus_mist
	if not mist:
		cloud = "cumulus"
	else:
		cloud = "cumulus_mist"

	# Direction
	x = cos(direction)*location
	y = sin(direction)*location
	# Location
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[0] = x
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[1] = y
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[0] = -x/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[1] = -y/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[2] = -location/2
	# Coverage
	cov = (0.6*coverage+20)/100
	black = 0.9*(1-cov)
	white = black+0.1
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[1].position = white
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Remover'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Remover'].color_ramp.elements[1].position = white
	# Density
	den = density/6666
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Density'].inputs[1].default_value = den
	# Mist
	gd = sky_calculation.gd
	m = (0.1*gd/103)/2
	if mist:
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Mist'].inputs[1].default_value = m


def cumulus_billboard_update(context):
	settings = context.scene.clouds_settings
	sky_en = context.scene.sky_settings.enabled
	direction = settings.cumulus_direction
	location = settings.cumulus_location*100
	coverage = settings.cumulus_coverage
	density = settings.cumulus_density
	cloud="cumulus_billboard"

	# Direction
	x = cos(direction)*location
	y = sin(direction)*location
	# Location
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[0] = x
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location'].inputs[1].default_value[1] = y
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[0] = -x/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[1] = -y/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover'].inputs[1].default_value[2] = -location/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Shadow'].inputs[1].default_value[0] = x
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Shadow'].inputs[1].default_value[1] = y
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover Shadow'].inputs[1].default_value[0] = -x/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover Shadow'].inputs[1].default_value[1] = -y/2
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Location Remover Shadow'].inputs[1].default_value[2] = -location/2
	# Coverage
	cov = coverage/100
	black = 0.9*(1-cov)
	white = black+0.1
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Coverage'].color_ramp.elements[1].position = white
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Remover'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Remover'].color_ramp.elements[1].position = white
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Remover Shadow'].color_ramp.elements[0].position = black
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Remover Shadow'].color_ramp.elements[1].position = white
	# Density
	den = 0.95*(1-density/100)+0.05
	bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Clouds Color'].color_ramp.elements[1].position = den
	if sky_en:
		# Sun Position
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Sun Position'].outputs[0].default_value = sky_calculation.vec
		# Clouds Color
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Clouds Color'].color_ramp.elements[0].color = sun_calculation.rgb
		# Clouds Strength
		gnd = sky_calculation.gnd
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Clouds Emission'].inputs[1].default_value = gnd*8
		# Mist
		bpy.data.objects[cloud].data.materials[0].node_tree.nodes['Mist'].inputs[1].default_value = gnd*2


# Calculation of the day of the year
def calculate_day(context):
	settings = context.scene.sky_settings
	month = settings.month
	if month==2:
		d = settings.day28
	if month in (4, 6, 9, 11):
		d = settings.day30
	if month in (1, 3, 5, 7, 8, 10, 12):
		d = settings.day31
	if month==1:
		day = d
	elif month==2:
		day = 31+d
	elif month==3:
		day = 59+d
	elif month==4:
		day = 90+d
	elif month==5:
		day = 120+d
	elif month==6:
		day = 151+d
	elif month==7:
		day = 181+d
	elif month==8:
		day = 212+d
	elif month==9:
		day = 243+d
	elif month==10:
		day = 273+d
	elif month==11:
		day = 304+d
	elif month==12:
		day = 334+d
	return day


# Handler
def sun_calculation_handler(self, context):
	sun_calculation(context)
def cirrus_update_handler(self, context):
	cirrus_update(context)
def cirrocumulus_update_handler(self, context):
	cirrocumulus_update(context)
def altostratus_selector_handler(self, context):
	altostratus_selector(context)
def cumulus_selector_handler(self, context):
	cumulus_selector(context)

@persistent
def sky_handler(scene):
	sky = scene.sky_settings
	if sky.enabled:
		sun_calculation(bpy.context)
	clouds = scene.clouds_settings
	if clouds.cirrus:
		cirrus_update(bpy.context)
	if clouds.cirrocumulus:
		cirrocumulus_update(bpy.context)
	if clouds.altostratus:
		altostratus_selector(bpy.context)
	if clouds.cumulus:
		cumulus_selector(bpy.context)


# Render engine handler
@persistent
def sky_handler1(*args):
	scene = bpy.context.scene
	sky = scene.sky_settings
	if sky.enabled:
		sun_calculation(bpy.context)
	clouds = scene.clouds_settings
	if clouds.cirrus:
		cirrus_update(bpy.context)
	if clouds.cirrocumulus:
		cirrocumulus_update(bpy.context)
	if clouds.altostratus:
		altostratus_selector(bpy.context)
	if clouds.cumulus:
		cumulus_selector(bpy.context)


#################################################################################
# Properties
class SkySettings(PropertyGroup):
	# Enable Sky
	enabled : BoolProperty(
		name = "Enable Sky",
		description = "Enable sky",
		default = False,
		update = enable_sky
		)

	# Sun
	direction : FloatProperty(
		name = "North",
		description = "North direction: at 0° the North is in the Y+ axis",
		min = 0,
		max = 2*pi,
		step = 100,
		precision = 1,
		default = 0,
		unit = "ROTATION",
		update = sun_calculation_handler
		)

	month : IntProperty(
		name = "Month",
		description = "Month number from 1 to 12",
		min = 1,
		max = 12,
		step = 1,
		default = 1,
		update = sun_calculation_handler
		)

	day28 : IntProperty(
		name = "Day",
		description = "Day number from 1 to 28",
		min = 1,
		max = 28,
		step = 1,
		default = 1,
		update = sun_calculation_handler
		)

	day30 : IntProperty(
		name = "Day",
		description = "Day number from 1 to 30",
		min = 1,
		max = 30,
		step = 1,
		default = 1,
		update = sun_calculation_handler
		)

	day31 : IntProperty(
		name = "Day",
		description = "Day number from 1 to 31",
		min = 1,
		max = 31,
		step = 1,
		default = 1,
		update = sun_calculation_handler
		)

	time : FloatProperty(
		name = "Time",
		description = "Time in hours",
		min = 0,
		max = 23.99,
		step = 1,
		precision = 2,
		default = 12,
		unit = "TIME",
		update = sun_calculation_handler
		)

	latitude : FloatProperty(
		name = "Latitude",
		description = "Local Latitude angle",
		min = -pi/2,
		max = pi/2,
		step = 10,
		precision = 3,
		default = 0.785,
		unit = "ROTATION",
		update = sun_calculation_handler
		)

	# Sky
	sky_method : EnumProperty(
		name = "Method",
		items=[
			('Real Sky', 'Real Sky', 'Real Sky', '', 0),
			('Hosek-Wilkie', 'Hosek-Wilkie', 'Hosek-Wilkie', '', 1)],
		description = "Select a sky method",
		default = "Real Sky",
		update = enable_sky
		)

	altitude : FloatProperty(
		name = "Altitude",
		description = "Altitude in meters above the sea level",
		min = 0,
		max = 2500,
		step = 100,
		precision = 3,
		default = 1,
		unit = "LENGTH",
		update = sun_calculation_handler
		)

	turbidity : IntProperty(
		name = "Turbidity",
		description = "Pollution of the air",
		min = 0,
		max = 100,
		default = 22,
		subtype = 'PERCENTAGE',
		update = sun_calculation_handler
		)

	albedo : IntProperty(
		name = "Albedo",
		description = "Reflected light on the ground",
		min = 0,
		max = 100,
		default = 30,
		subtype = 'PERCENTAGE',
		update = sun_calculation_handler
		)


class CloudsSettings(PropertyGroup):
	# Cirrus
	cirrus : BoolProperty(
		name = "Cirrus",
		description = "Enable Cirrus type",
		default = False,
		update = enable_cirrus
		)

	cirrus_direction : FloatProperty(
		name = "Wind",
		description = "Wind direction",
		min = 0,
		max = 2*pi,
		step = 100,
		precision = 1,
		default = 0,
		unit = "ROTATION",
		update = cirrus_update_handler
		)

	cirrus_location : FloatProperty(
		name = "Location",
		description = "Cirrus clouds location",
		min = 0,
		max = float("inf"),
		step = 1,
		default = 0,
		update = cirrus_update_handler
		)

	cirrus_coverage : IntProperty(
		name = "Coverage",
		description = "Cirrus clouds coverage",
		min = 0,
		max = 100,
		default = 50,
		subtype = 'PERCENTAGE',
		update = cirrus_update_handler
		)

	cirrus_density : IntProperty(
		name = "Density",
		description = "Cirrus clouds density",
		min = 0,
		max = 100,
		default = 100,
		subtype = 'PERCENTAGE',
		update = cirrus_update_handler
		)

	# Cirrocumulus
	cirrocumulus : BoolProperty(
		name = "Cirrocumulus",
		description = "Enable Cirrocumulucs type",
		default = False,
		update = enable_cirrocumulus
		)

	cirrocumulus_direction : FloatProperty(
		name = "Wind",
		description = "Wind direction",
		min = 0,
		max = 2*pi,
		step = 100,
		precision = 1,
		default = 0,
		unit = "ROTATION",
		update = cirrocumulus_update_handler
		)

	cirrocumulus_location : FloatProperty(
		name = "Location",
		description = "Cirrocumulus clouds location",
		min = 0,
		max = float("inf"),
		step = 1,
		default = 0,
		update = cirrocumulus_update_handler
		)

	cirrocumulus_coverage : IntProperty(
		name = "Coverage",
		description = "Cirrocumulus clouds coverage",
		min = 0,
		max = 100,
		default = 50,
		subtype = 'PERCENTAGE',
		update = cirrocumulus_update_handler
		)

	cirrocumulus_density : IntProperty(
		name = "Density",
		description = "Cirrocumulus clouds density",
		min = 0,
		max = 100,
		default = 100,
		subtype = 'PERCENTAGE',
		update = cirrocumulus_update_handler
		)

	# Altostratus
	altostratus : BoolProperty(
		name = "Altostratus",
		description = "Enable Altostratus type",
		default = False,
		update = enable_altostratus
		)

	altostratus_method : EnumProperty(
		description = "Altostratus method",
		items=[
		('Volume', 'Volume', 'Volume', '', 0),
		('Billboard', 'Billboard', 'Billboard', '', 1)],
		default = 'Volume',
		update = enable_altostratus
		)

	altostratus_billboard_res : EnumProperty(
		description = "Billboard resolution",
		items=[
		('Low', 'Low', 'Low', '', 0),
		('Mid', 'Mid', 'Mid', '', 1),
		('High', 'High', 'High', '', 2)],
		default = 'Low',
		update = enable_altostratus
		)

	altostratus_mist : BoolProperty(
		name = "Mist",
		description = "Enable Mist",
		default = True,
		update = enable_altostratus
		)

	altostratus_direction : FloatProperty(
		name = "Wind",
		description = "Wind direction",
		min = 0,
		max = 2*pi,
		step = 100,
		precision = 1,
		default = 0,
		unit = "ROTATION",
		update = altostratus_selector_handler
		)

	altostratus_location : FloatProperty(
		name = "Location",
		description = "Altostratus clouds location",
		min = 0,
		max = float("inf"),
		step = 1,
		default = 0,
		update = altostratus_selector_handler
		)

	altostratus_coverage : IntProperty(
		name = "Coverage",
		description = "Altostratus clouds coverage",
		min = 0,
		max = 100,
		default = 50,
		subtype = 'PERCENTAGE',
		update = altostratus_selector_handler
		)

	altostratus_density : IntProperty(
		name = "Density",
		description = "Altostratus clouds density",
		min = 0,
		max = 100,
		default = 50,
		subtype = 'PERCENTAGE',
		update = altostratus_selector_handler
		)

	# Cumulus
	cumulus : BoolProperty(
		name = "Cumulus",
		description = "Enable Cumulus type",
		default = False,
		update = enable_cumulus
		)

	cumulus_method : EnumProperty(
		description = "Cumulus method",
		items=[
		('Volume', 'Volume', 'Volume', '', 0),
		('Billboard', 'Billboard', 'Billboard', '', 1)],
		default = 'Volume',
		update = enable_cumulus
		)

	cumulus_billboard_res : EnumProperty(
		description = "Billboard resolution",
		items=[
		('Low', 'Low', 'Low', '', 0),
		('Mid', 'Mid', 'Mid', '', 1),
		('High', 'High', 'High', '', 2)],
		default = 'Low',
		update = enable_cumulus
		)

	cumulus_mist : BoolProperty(
		name = "Mist",
		description = "Enable Mist",
		default = True,
		update = enable_cumulus
		)

	cumulus_direction : FloatProperty(
		name = "Wind",
		description = "Wind direction",
		min = 0,
		max = 2*pi,
		step = 100,
		precision = 1,
		default = 0,
		unit = "ROTATION",
		update = cumulus_selector_handler
		)

	cumulus_location : FloatProperty(
		name = "Location",
		description = "Cumulus clouds location",
		min = 0,
		max = float("inf"),
		step = 1,
		default = 0,
		update = cumulus_selector_handler
		)

	cumulus_coverage : IntProperty(
		name = "Coverage",
		description = "Cumulus clouds coverage",
		min = 0,
		max = 100,
		default = 50,
		subtype = 'PERCENTAGE',
		update = cumulus_selector_handler
		)

	cumulus_density : IntProperty(
		name = "Density",
		description = "Cumulus clouds density",
		min = 0,
		max = 100,
		default = 50,
		subtype = 'PERCENTAGE',
		update = cumulus_selector_handler
		)


#################################################################################
classes = (
	REALSKY_PT_Sky,
	REALSKY_PT_Clouds,
	SkySettings,
	CloudsSettings
	)

register, unregister = bpy.utils.register_classes_factory(classes)

# Register
def register():
	for cls in classes:
		bpy.utils.register_class(cls)
	bpy.types.Scene.sky_settings = PointerProperty(type=SkySettings)
	bpy.types.Scene.clouds_settings = PointerProperty(type=CloudsSettings)
	# Libraries
	spectrum.register()
	billboard.register()
	# Icons
	global clouds_icons
	clouds_icons = previews.new()
	icons_dir = os.path.join(os.path.dirname(__file__), "icons")
	clouds_icons.load("cirrus", os.path.join(icons_dir, "cirrus.png"), 'IMAGE')
	clouds_icons.load("cirrocumulus", os.path.join(icons_dir, "cirrocumulus.png"), 'IMAGE')
	clouds_icons.load("altostratus", os.path.join(icons_dir, "altostratus.png"), 'IMAGE')
	clouds_icons.load("cumulus", os.path.join(icons_dir, "cumulus.png"), 'IMAGE')
	# Frame change handler
	bpy.app.handlers.frame_change_post.append(sky_handler)
    
def unregister():
	# Icons
	global clouds_icons
	previews.remove(clouds_icons)
	# Libraries
	spectrum.unregister()
	billboard.unregister()
	# Addon
	for cls in classes:
		bpy.utils.unregister_class(cls)
	del bpy.types.Scene.sky_settings
	del bpy.types.Scene.clouds_settings
	# Handlers
	bpy.app.handlers.frame_change_post.remove(sky_handler)

if __name__ == "__main__":
	register()
